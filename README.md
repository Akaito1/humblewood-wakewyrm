Humblewood: The Wakewyrm's Fury
===============================

This is an unofficial preparation of The Deck of Many's Free RPG Day content
"The Wakewyrm's Fury", in their Humblewood setting.
Go to [Humblewood.com](https://humblewood.com/) to learn more.

Humblewood: The Wakewyrm's Fury is Copyright © 2020 Hit Point Press Inc.

